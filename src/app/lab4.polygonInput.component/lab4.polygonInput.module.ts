import { Lab4PolygonInputComponent } from "./lab4.polygonInput.component";
import { NgModule } from "@angular/core";
import {
    MatCardModule, 
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
} from "@angular/material"
import { RouterModule } from "@angular/router";
import{
    FormsModule
}from "@angular/forms"
@NgModule({
    declarations:[
        Lab4PolygonInputComponent
    ],
    imports:[
        MatCardModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        MatIconModule,
        MatInputModule,
        RouterModule
    ],
    providers:[],
    entryComponents:[Lab4PolygonInputComponent],
    exports:[Lab4PolygonInputComponent]
    
})
export class Lab4PolygonInputModule{}