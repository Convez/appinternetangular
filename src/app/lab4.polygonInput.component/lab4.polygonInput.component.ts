import {Component} from '@angular/core'

@Component({
    selector: 'lab4-polygonInput',
    templateUrl: './lab4.polygonInput.component.html',
    styleUrls: ['./lab4.polygonInput.component.scss']
  })
export class Lab4PolygonInputComponent{
    headerText="Insert a polygon"
}