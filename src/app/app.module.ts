import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from "@angular/forms"
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {Lab4MapModule} from "./lab4.map.component/lab4.map.module"
import {Lab4SidebarModule} from "./lab4.sidebar.component/lab4.sidebar.module"
import {Lab4LoginModule} from "./lab4.login.component/lab4.login.module"
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app.routing.module';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { Lab4ServerRequestsService } from './lab4.serverRequests.service';
import { RouterModule } from '@angular/router';
import { Lab4NotFoundModule } from './lab4.notfound.component/lab4.notfound.module';
import { Lab4RegisterModule } from './lab4.register.component/lab4.register.module';
import { Lab4PositionSearchModule } from './lab4.positionSearch.component/lab4.positionSearch.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule,
    Lab4SidebarModule,
    Lab4LoginModule,
    Lab4NotFoundModule,
    Lab4RegisterModule,
    Lab4PositionSearchModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [Lab4ServerRequestsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
