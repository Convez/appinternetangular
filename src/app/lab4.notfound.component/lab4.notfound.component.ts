import { Component } from "@angular/core";

@Component({
    selector:"lab4-notfound",
    templateUrl:"./lab4.notfound.component.html",
    styleUrls:["./lab4.notfound.component.scss"]
})
export class Lab4NotFoundComponent{}