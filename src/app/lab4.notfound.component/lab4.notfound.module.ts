import { NgModule } from "@angular/core";
import { RouterModule, Router } from "@angular/router"
import { Lab4NotFoundComponent } from "./lab4.notfound.component";
@NgModule({
    declarations:[Lab4NotFoundComponent],
    imports:[
        RouterModule
    ],
    exports:[Lab4NotFoundComponent]
})
export class Lab4NotFoundModule{}