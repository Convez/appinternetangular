import { Lab4SidebarComponent } from "./lab4.sidebar.component";
import { NgModule } from "@angular/core";
import {MatToolbarModule} from "@angular/material";
import { RouterModule } from "@angular/router";
import { MatSidenavModule, MatButtonModule, MatMenuModule } from '@angular/material';

@NgModule({
    declarations:[
        Lab4SidebarComponent
    ],
    imports:[
        MatToolbarModule,
        RouterModule,
        MatSidenavModule,
        MatButtonModule,
        MatMenuModule
    ],
    providers:[],
    entryComponents:[],
    exports:[Lab4SidebarComponent]
    
})
export class Lab4SidebarModule{}