import {Component} from '@angular/core'

@Component({
    selector: 'lab4-login',
    templateUrl: './lab4.login.component.html',
    styleUrls: ['./lab4.login.component.scss']
  })
export class Lab4LoginComponent{
    headerText="Log In"
}