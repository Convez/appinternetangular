import { Lab4LoginComponent } from "./lab4.login.component";
import { NgModule } from "@angular/core";
import {
    MatCardModule, 
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
} from "@angular/material"
import { RouterModule } from "@angular/router";
import{
    FormsModule
}from "@angular/forms"
@NgModule({
    declarations:[
        Lab4LoginComponent
    ],
    imports:[
        MatCardModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        MatIconModule,
        MatInputModule,
        RouterModule
    ],
    providers:[],
    entryComponents:[],
    exports:[Lab4LoginComponent]
    
})
export class Lab4LoginModule{}