import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { element } from "protractor";
import { MatPaginator, MatSnackBar, MatProgressBar } from "@angular/material";
import { tap } from "rxjs/operators";
import { Lab4PositionSearchService } from "./lab4.positionSearch.service";

@Component({
    selector: 'lab4-pos-search',
    templateUrl: './lab4.positionSearch.component.html',
    styleUrls: ['./lab4.positionSearch.component.scss']
  })
export class Lab4PositionSearch implements OnInit{
  @ViewChild("positionsPaginator") paginator:MatPaginator;
  progressBarHidden:boolean = true;
  dateFrom:Date = new Date();
  dateTo:Date = new Date();
  constructor(
    private positionSearchService:Lab4PositionSearchService,
    private snackbar:MatSnackBar,
    public cdRef:ChangeDetectorRef
  ){}
  ngOnInit(){
    this.dateTo.setDate(this.dateFrom.getDate()+1)
    this.data.forEach((element)=>element.timestamp=this.dateFrom.valueOf());
    this.tableRowsSize = this.data.length;
    this.positionSearchService.setpositionSearchComponent(this);
    this.paginator.pageIndex=0;
    this.paginator.pageSize=5;
    this.paginator.page.pipe(
      tap(()=>this.loadPage())
    ).subscribe()
    this.loadPage();
  }
  loadPage = ()=>{
    this.paginator.length=this.data.length;
    let startIndex = this.paginator.pageIndex*this.paginator.pageSize;
    let endIndex = startIndex+this.paginator.pageSize;
    this.dataSource = this.data.slice(startIndex,endIndex<this.data.length?endIndex:this.data.length)
  }
  tableRowsSize = 60;

  displayedColumns = ['longitude', 'latitude', 'cost'];
  dataSource = [];
  data = [];
  onDateChanged = (event) => {
    if (this.positionSearchService.searchPositions()==false){
      this.progressBarHidden=true;
      this.snackbar.open("First draw a polygon","OK",{duration:2000});
    }
  }
  addPoint = (point)=>{
    if (!this.data.includes(point)){
      this.data.push(point);
    }
  }
  getTotalCost = ()=>{
    let total=0.0;
    this.data.forEach((element)=>total+=parseFloat(element.properties.cost))
    return total;
  }
  setupLoading=()=>{
    this.data=[];
    this.dataSource=[];
    this.paginator.pageIndex=0;
    this.progressBarHidden=false;
    this.cdRef.detectChanges();
  }
}