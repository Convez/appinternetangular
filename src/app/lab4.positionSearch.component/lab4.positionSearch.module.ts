import { NgModule } from "@angular/core";
import { Lab4PositionSearch } from "./lab4.positionSearch.component";
import {Lab4MapModule} from "../lab4.map.component/lab4.map.module"
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatCardModule, MatPaginatorModule, MatTableModule, MatSnackBarModule, MatProgressBarModule } from "@angular/material";
import { FormsModule } from "@angular/forms";
import { Lab4PositionSearchService } from "./lab4.positionSearch.service";
import { CommonModule } from "@angular/common";


@NgModule({
    declarations:[
        Lab4PositionSearch
    ],
    imports:[
        Lab4MapModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatCardModule,
        MatInputModule,
        MatPaginatorModule,
        MatTableModule,
        MatSnackBarModule,
        MatProgressBarModule,
        CommonModule,
        FormsModule
    ],
    providers:[Lab4PositionSearchService],
    entryComponents:[],
    exports:[Lab4PositionSearch]
    
})
export class Lab4PositionSearchModule{}