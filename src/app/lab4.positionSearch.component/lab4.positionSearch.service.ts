import { Injectable, ChangeDetectorRef } from "@angular/core";
import { Lab4PositionSearch } from "./lab4.positionSearch.component";
import { Lab4ServerRequestsService } from "../lab4.serverRequests.service";
import * as L from "geojson";
@Injectable()
export class Lab4PositionSearchService{
    private positionSearchComponent:Lab4PositionSearch;
    private lastPolygon:L.Feature<any>;
    constructor(
        private serverRequestsService:Lab4ServerRequestsService
    ){}
    setpositionSearchComponent=(component:Lab4PositionSearch)=>this.positionSearchComponent=component;
    
    searchPositions = (inside:L.Feature<any>=undefined):boolean=>{
        let from = this.positionSearchComponent.dateFrom;
        let to = this.positionSearchComponent.dateTo;
        this.positionSearchComponent.setupLoading();
        if(inside==undefined)
            if(this.lastPolygon==undefined)
                return false;
            else
                inside=this.lastPolygon;   
        else
            this.lastPolygon=inside;
        let positionSearch = this.serverRequestsService
        .getPositionsByPolygonWithinAndTimestampBetween(inside,from,to)
        .forEach(point=>this.positionSearchComponent.addPoint(point))
        .then(()=>{
            this.positionSearchComponent.loadPage();
            this.positionSearchComponent.progressBarHidden=true;
            this.positionSearchComponent.cdRef.detectChanges();
        });
        return true;
        
    }

}