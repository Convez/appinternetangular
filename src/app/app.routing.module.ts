import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { Lab4LoginModule } from './lab4.login.component/lab4.login.module';
import { Lab4LoginComponent } from './lab4.login.component/lab4.login.component';
import { Lab4MapComponent } from './lab4.map.component/lab4.map.component';
import { Lab4MapModule } from './lab4.map.component/lab4.map.module';
import { Lab4NotFoundComponent } from './lab4.notfound.component/lab4.notfound.component';
import { Lab4NotFoundModule } from './lab4.notfound.component/lab4.notfound.module';
import { Lab4RegisterComponent } from './lab4.register.component/lab4.register.component';
import { Lab4RegisterModule } from './lab4.register.component/lab4.register.module';
import { HttpClientModule } from '@angular/common/http';
import { Lab4PositionSearchModule } from './lab4.positionSearch.component/lab4.positionSearch.module';
import { Lab4PositionSearch } from './lab4.positionSearch.component/lab4.positionSearch.component';

@NgModule({
  declarations: [ 
  ],
  imports: [
      Lab4LoginModule,
      Lab4PositionSearchModule,
      Lab4NotFoundModule,
      Lab4RegisterModule,
      HttpClientModule,
    RouterModule.forRoot([
      { path: 'home', component: Lab4PositionSearch },
      { path: 'login', component: Lab4LoginComponent },
      { path: 'register', component: Lab4RegisterComponent },
      {path:"", redirectTo:"home", pathMatch:"full"},
      { path: '**', component:Lab4NotFoundComponent},
    ])
  ],
  exports: [
    RouterModule,
  ],
  providers: [],

})
export class AppRoutingModule {}


