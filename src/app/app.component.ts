import { Component, ViewChild, OnInit } from '@angular/core';
import * as L from "leaflet"
import { MatDrawer, MatButton } from '@angular/material';
import { Router,NavigationEnd, NavigationCancel, NavigationError, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  @ViewChild("sideMenu") sidenav:MatDrawer;
  @ViewChild("menuButton") menuButton:MatButton;
  constructor(private router:Router){
  }
  ngOnInit(){
    this.router.events 
            .subscribe((event) => {
                if(event instanceof NavigationStart
                ){
                  if(this.sidenav.opened)
                    this.sidenav.toggle();
                    this.menuButton._elementRef.nativeElement.classList.remove('cdk-program-focused');
                    this.menuButton._elementRef.nativeElement.classList.remove('cdk-mouse-focused');
                }
            });
  }
  public onSidenavClose=()=>{
    this.menuButton._elementRef.nativeElement.classList.remove('cdk-program-focused');
    this.menuButton._elementRef.nativeElement.classList.remove('cdk-mouse-focused');
  }
  public menuButtonClick=()=>{
    this.sidenav.toggle();
  }

  title = 'app';
}
