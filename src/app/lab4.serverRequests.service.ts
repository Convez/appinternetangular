import { Injectable} from "@angular/core";
import * as Geo from "geojson";
import {HttpModule} from "@angular/http"
import { HttpClient } from "@angular/common/http";
import * as Operators from 'rxjs/operators';
import { HttpResponse } from "selenium-webdriver/http";
import {Observable} from 'rxjs'

@Injectable()
export class Lab4ServerRequestsService{
  constructor(private http:HttpClient){}
    getPositionsByPolygonWithinAndTimestampBetween = (
        polygon,
        from: Date,
        to: Date
    ):Observable<Geo.Feature<Geo.Point>> => {
      console.log("boh")
      return this.http
        .get("assets/positions.json")
        .pipe(
          Operators.delay(1000),
          Operators.flatMap((points:Geo.FeatureCollection<Geo.Point>)=>points.features),
          Operators.filter((point:Geo.Feature<Geo.Point>)=>this.isPointInPolygon(point,polygon)),
          Operators.filter((point:Geo.Feature<Geo.Point>) => this.isPointTimestampInDates(point,from,to))
        );
    }
    checkPointsInPolygon = (points,polygon)=>{
      console.log("polygon");
      console.log(polygon);
        let toReturn = [];
        for(;points.features.length>0;){
            let pnt = points.features.pop();
            if(this.isPointInPolygon(pnt,polygon)){
              toReturn.push(pnt);
            }
        }
        return toReturn;
    }
    isPointInPolygon = (point,vs)=>{
        point = point.geometry.coordinates;
        let polygon = vs.geometry.coordinates[0];
        let x = point[1], y = point[0];
        let inside = false;
        
        for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
            let xi = polygon[i][0], yi = polygon[i][1];
            let xj = polygon[j][0], yj = polygon[j][1];
            let intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }
        return inside;
    }
    isPointTimestampInDates = (point:Geo.Feature<Geo.Point>,from:Date,to:Date)=>{
        return point.properties.timestamp>=from.valueOf() && 
            point.properties.timestamp<=to.valueOf();
    }
    
}