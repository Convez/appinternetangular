import { NgModule } from "@angular/core";
import { Lab4MapComponent } from "./lab4.map.component";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import {LeafletDrawModule} from "@asymmetrik/ngx-leaflet-draw"
import { MatDialogModule } from "@angular/material";
import { Lab4PolygonInputModule } from "../lab4.polygonInput.component/lab4.polygonInput.module";


@NgModule({
    declarations:[
        Lab4MapComponent
    ],
    imports:[
        LeafletModule.forRoot(),
        LeafletDrawModule.forRoot(),
        MatDialogModule,
        Lab4PolygonInputModule
    ],
    providers:[],
    entryComponents:[],
    exports:[Lab4MapComponent]
    
})
export class Lab4MapModule{}