import { Component, ViewChild, OnInit, Input } from '@angular/core';
import * as L from "leaflet"
import { MatSlider, MatDatepicker, MatDatepickerModule, MatDialog } from '@angular/material';
import * as $ from "jquery";
import "jqueryui";
import { Lab4ServerRequestsService } from '../lab4.serverRequests.service';
import { Lab4PositionSearchService } from '../lab4.positionSearch.component/lab4.positionSearch.service';
import { Lab4PolygonInputComponent } from '../lab4.polygonInput.component/lab4.polygonInput.component';

@Component({
  selector: 'lab4-map',
  templateUrl: './lab4.map.component.html',
  styleUrls: ['./lab4.map.component.scss']
})
export class Lab4MapComponent{
  public map:L.Map;
  private lastLayer:L.Layer;
  private lastMarkers:L.Marker[];
  private userMarker:L.Marker;
  private alreadySet = false;
  constructor(
    private positionSearchService:Lab4PositionSearchService,
    private dialog:MatDialog
  ){}
  drawOptions = {
    position: 'topright',
    draw: {
      polyline: false,
      circle: false,
      marker: false,
      circlemarker: false
    }
  };

  options = {
      layers: [
        L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
          maxZoom: 20,
          subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
          detectRetina: true
        }),
      ],
      bounds: L.latLngBounds(
        L.latLng(0, -180),
        L.latLng(85, 0)
      ),
      zoom: 7,
      center: L.latLng([ 46.879966, -121.726909 ])
    };
    
  mapReadyCallback = (map:L.Map) : void => {
    this.map=map;


    var southWest = L.latLng(0, -180),
    northEast = L.latLng(85, 0),
    bounds = L.latLngBounds(southWest, northEast);
    this.map.fitBounds(bounds);

    map.on(L.Draw.Event.CREATED, this.polygonDrawedCallback);
    //Start location
    let options = {
      setView:false,
      maxZoom:16,
      watch:true
    }
    this.map.on("locationfound",this.locationFoundCallback)
    this.map = this.map.locate(options);
    //Add center control
    var centerMapControl = L.Control.extend({
      options: {
      position: 'bottomright' 
      //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
      },
      onAdd: (map)=> {
        var container = L.DomUtil.create('button', 'material-icons md-24 leaflet-bar leaflet-control leaflet-control-custom');
          
          container.style.width = '30px';
          container.style.height = '30px';
          container.innerText="my_location";
          container.style.backgroundColor = 'white';
          container.style.display="inline block;"
          container.style.padding="0px";
          container.style.cursor="pointer";
          container.onclick = (event)=>{this.centerMap(container);}
          return container;
        },
      });
      var polygonUploadControl = L.Control.extend({
        options: {
        position: 'bottomleft' 
        //control position - allowed: 'topleft', 'topright', 'bottomleft', 'bottomright'
        },
        onAdd: (map)=> {
          var container = L.DomUtil.create('button', 'material-icons md-24 leaflet-bar leaflet-control leaflet-control-custom');
            
            container.style.width = '30px';
            container.style.height = '30px';
            container.innerText="cloud_upload";
            container.style.backgroundColor = 'white';
            container.style.display="inline block;"
            container.style.padding="0px";
            container.style.cursor="pointer";
            container.onclick = (event)=>{
              container.blur();
              this.dialog.open(Lab4PolygonInputComponent,{
               "width":"350px",
               "height":"350px"
              })
            }
            return container;
          },
        });
      this.map.addControl(new centerMapControl());
      this.map.addControl(new polygonUploadControl());
  }

  onSubmit =(container)=>{
  }

  centerMap = (container)=>{
    container.blur();
    if(this.map!=undefined && this.userMarker!=undefined){
      this.map.invalidateSize();
      this.map.setView(this.userMarker.getLatLng(),16);
    }
  }
  locationFoundCallback = (e:L.LocationEvent)=>this.drawUserMarker(e.latlng);

  drawUserMarker = (latlng:L.LatLng)=>{
    if(this.userMarker != undefined){
      this.userMarker.removeFrom(this.map);
    }
    let marker = L.marker(latlng);
    marker.setIcon(L.icon(
      {
        iconSize: [ 32, 32 ],
        iconAnchor: [ 13, 41 ],
        iconUrl: 'assets/position-marker.png'
      }
    ));
    marker.addTo(this.map);
    if(!this.alreadySet){
      this.map.setView(latlng,16);
      this.map.invalidateSize();
      this.alreadySet = true;
    }
    this.userMarker = marker;
  };

  polygonDrawedCallback = (e:L.DrawEvents.Created)=>{
    let type = e.layerType, layer = e.layer;
    if(this.lastLayer!=undefined)
      this.lastLayer.removeFrom(this.map);
    if(this.lastMarkers!=undefined)
      this.lastMarkers.forEach((marker)=>marker.removeFrom(this.map))
    this.lastMarkers = [];
    layer.addTo(this.map);
    this.lastLayer = layer;
    this.positionSearchService.searchPositions(layer.toGeoJSON());

  }
}