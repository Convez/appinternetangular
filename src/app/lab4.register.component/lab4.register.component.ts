import {Component, OnInit} from '@angular/core'
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { patternValidator, errorMessages,CustomValidators, ConfirmValidParentMatcher, regExps } from './lab4.register.validators';

@Component({
    selector: 'lab4-register',
    templateUrl: './lab4.register.component.html',
    styleUrls: ['./lab4.register.component.scss']
  })
export class Lab4RegisterComponent implements OnInit{

    headerText="Register"
    userRegistrationForm: FormGroup;
    errors = errorMessages;
    confirmValidParentMatcher = new ConfirmValidParentMatcher();
    ngOnInit(): void {
        this.createForm();
    }
    
    createForm = ()=>{
        this.userRegistrationForm = new FormGroup({
            emailGroup: new FormGroup({
                email: new FormControl('', [Validators.required, Validators.email]),
                confirmEmail: new FormControl('',Validators.required)
            },{validators:CustomValidators.childrenEqual}),
            passwordGroup: new FormGroup({
                password: new FormControl('', [Validators.required, 
                    Validators.pattern(regExps.password)]),
                confirmPassword: new FormControl('',Validators.required)
            },{validators: CustomValidators.childrenEqual})
        });
    }
    
}