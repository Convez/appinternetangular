import { NgModule } from "@angular/core";
import {
    MatCardModule, 
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
} from "@angular/material"
import { RouterModule } from "@angular/router";
import{
    FormsModule
}from "@angular/forms"
import { Lab4RegisterComponent } from "./lab4.register.component";
import {ReactiveFormsModule} from "@angular/forms"
@NgModule({
    declarations:[
        Lab4RegisterComponent
    ],
    imports:[
        MatCardModule,
        MatButtonModule,
        MatFormFieldModule,
        FormsModule,
        MatIconModule,
        MatInputModule,
        RouterModule,
        ReactiveFormsModule
    ],
    providers:[],
    entryComponents:[],
    exports:[Lab4RegisterComponent]
    
})
export class Lab4RegisterModule{}