# Lab4Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

## Dependencies
Angular material: run `npm install --save @angular/material @angular/cdk`  
Angular material extended: run `npm install @material-extended/mde`
Angular animations: run `npm install --save @angular/animations`  
Angular gestures support: run `npm install --save hammerjs`  
Leaflet: run `npm install leaflet` and `npm install @asymmetrik/ngx-leaflet`  
Leaflet typescript: run `npm install --save-dev @types/leaflet`  
Leaflet draw: run `npm install leaflet-draw` and `npm install @asymmetrik/ngx-leaflet-draw`  
Leaflet typescript: run `npm install --save-dev @types/leaflet-draw`  

Jquery: run  `npm install jquery` and `npm install --save-dev @types/jquery`  
JqueryUI: run `npm install jqueryui` and `npm install --save @types/jqueryui`  
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
